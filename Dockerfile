FROM node:8-slim

WORKDIR /app

# best practice
COPY package.json .

RUN  npm install

# copy source-code
COPY . .

# kubernetes command
ENTRYPOINT [ "node","server.js" ]

# k8 args
#CMD [ "executable" ]

EXPOSE 3000

